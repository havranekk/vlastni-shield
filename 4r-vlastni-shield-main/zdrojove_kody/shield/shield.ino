#include "leds.h"
#include "display.h"
#include "temp-sensor.h"
#include "rgb-strip.h"
#include "photo.h"

void setup() {
  red();
  displaySetup();
  ledSetup();
  photoSetup();
  rgbSetup();
  tempSetup();
  green();
}

void loop() {
  vypis(getTemp());
  svetlo(getPhoto());
}

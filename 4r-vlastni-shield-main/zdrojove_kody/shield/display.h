#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

void displaySetup() {
  lcd.init();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Test");
}

void vypis(float temp){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Teplota v C: ");
  lcd.setCursor(0, 1);
  lcd.print(temp);
}

//done

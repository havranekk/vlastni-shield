#include <DallasTemperature.h>
#include <OneWire.h>

#define ONE_WIRE_BUS D2

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

void tempSetup() {
  sensors.begin();
}

int getTemp(){
  sensors.requestTemperatures();
  return sensors.getTempCByIndex(0);
}

//done

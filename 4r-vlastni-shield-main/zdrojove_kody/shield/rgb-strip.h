#include <Adafruit_NeoPixel.h>

#define pinDIN D7
#define pocetLED 8

Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);

void nastavRGB (byte r, byte g, byte b, int cislo) {
  uint32_t barva;
  barva = rgbWS.Color(r, g, b);
  rgbWS.setPixelColor(cislo - 1, barva);
  rgbWS.show();
}

void rgbSetup() {
  rgbWS.begin();
  for (int i = 1; i < 9; i++) {
    nastavRGB(50, 0, 0, i);
  }
  delay(2000);
  for (int i = 1; i < 9; i++) {
    nastavRGB(0, 0, 0, i);
  }
  
  for (int i = 1; i < 9; i++) {
    nastavRGB(0, 50, 0, i);
  }
  delay(2000);
  for (int i = 1; i < 9; i++) {
    nastavRGB(0, 0, 0, i);
  }
  
  for (int i = 1; i < 9; i++) {
    nastavRGB(0, 0, 50, i);
  }
  delay(2000);
  for (int i = 1; i < 9; i++) {
    nastavRGB(0, 0, 0, i);
  }
}

void svetlo(int s){
  if(s >= 10){
    for (int i = 1; i < 9; i++) {
      nastavRGB(50, 50, 50, i);
    }
  }else{
    for (int i = 1; i < 9; i++) {
      nastavRGB(0, 0, 0, i);
    }
  }
}

const int R = D9;
const int G = D8;

void ledSetup() {
  pinMode(R, OUTPUT);
  pinMode(G, OUTPUT);
}

void red() {
  digitalWrite(G, LOW);
  digitalWrite(R, HIGH);
}

void green() {
  digitalWrite(R, LOW);
  digitalWrite(G, HIGH);
}

//done
